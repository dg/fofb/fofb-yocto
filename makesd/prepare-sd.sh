#!/bin/bash

OPTIND=1

image_name="zup-image-soleil-fofb-damc-fmc2zup"
copy_src="./"

##################################################
## Usage message
show_help () {
    echo "Usage:"
    echo "       prepare-sd.sh [opts] sdcard_path"
    echo "          opts"
    echo "           -h         Show help."
    echo "           -p         Write partition table."
    echo "           -f         Format partitions."
    echo "           -c         Copy files."
    echo "           -s src     Files source, default to ${copy_src}."
    echo "           -A         Do partition, format and copy."
    echo "           -i name    Image name, default to ${image_name}."
    echo ""
    echo "          sdcard_path  path to sd device."
}

##################################################
## Handle command line arguments
do_partition=0
do_format=0
do_copy=0

while getopts "hpfcs:Ai:" opt; do
    case $opt in
        h)
            show_help
            exit 0
            ;;
        p)
            do_partition=1
            ;;
        f)
            do_format=1
            ;;
        c)
            do_copy=1
            ;;
        s)
            copy_src=$OPTARG
            ;;
        A)
            do_partition=1
            do_format=1
            do_copy=1
            ;;
        i)
            image_name=$OPTARG
            ;;
    esac
done

##################################################
## Retreive SD device path from command line
shift $((OPTIND-1))
if [ $# -eq 0 ]
then
    echo "ERROR: Path to SD device is mandatory"
    show_help
    exit -1
fi
sd_path=$1

##################################################
## Make sure we are root user
if [[ "$EUID" != 0 ]]; then
    echo "Error: Needs to be super user root"
    exit -1
fi

##################################################
## Partition
if [ $do_partition -eq 1 ]
then
    echo "=========== Write new partition table to SD-card with sfdisk ==========="
    sfdisk $sd_path < sfdisk.input

    if [[ $? -ne 0 ]] ; then
        echo "Error during partition step"
        exit 1
    fi

    echo "====================== Write partition table done ======================"
    echo "="
fi

##################################################
## Formating
if [ $do_format -eq 1 ]
then
    echo "===================== Formating the partitions ========================="
    mkfs.vfat -F 32 -n boot ${sd_path}p1

    if [[ $? -ne 0 ]] ; then
        echo "Error during formating step"
        exit 1
    fi

    mkfs.ext4 -L root ${sd_path}p2

    if [[ $? -ne 0 ]] ; then
        echo "Error during formating step"
        exit 1
    fi

    echo "======================= Format patition done ==========================="
    echo "="
fi

##################################################
## File copy
if [ $do_copy -eq 1 ]
then
    echo "=============== Mounting and copying the SD card content ==============="
    mkdir -p /media/sd-boot
    mkdir -p /media/sd-root
    mount ${sd_path}p1 /media/sd-boot
    mount ${sd_path}p2 /media/sd-root

    cp ${copy_src}/boot.bin                             /media/sd-boot
    cp ${copy_src}/damc-fmc2zup-system.dtb              /media/sd-boot
    cp ${copy_src}/Image                                /media/sd-boot
    cp ${copy_src}/uEnv.txt                             /media/sd-boot
    if [[ -e ${copy_src}/download-damc-fmc2zup.bit ]]; then
        cp ${copy_src}/download-damc-fmc2zup.bit            /media/sd-boot/damc_fmc2zup_top.bit
    fi

    tar -xzf ${copy_src}/${image_name}.tar.gz -C /media/sd-root

    # The Following is copy from file copy
    #cp -r file-copy/boot/* /media/sd-boot/
    #cp -r file-copy/root/* /media/sd-root/

    sync
    umount /media/sd-boot /media/sd-root
    rmdir /media/sd-boot /media/sd-root
    echo "==================== File copied, SD card ready ========================"
fi
