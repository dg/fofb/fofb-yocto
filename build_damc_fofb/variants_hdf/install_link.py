#!/usr/bin/env python3

import argparse
import glob
import os
import re

##############################################
# UPDATE the following path

# Directory to locate simlinks, used by yocto recipes. Same link as PL_VARIANTS_DIR in local.conf
VARIANTS_DIR="/home/sources/diagnostics/grabas/yocto/fofb-yocto/build_damc_fofb/variants_hdf/"

# Directories where to find xsa files (fwk out dir)
OUT_DIRS=[
        "/home/sources/diagnostics/broucquart/FPGA_FOFB/fwk_fofbnode/out/",
        "/home/sources/diagnostics/broucquart/FPGA_FOFB/prod_fofbnode/out/",
        "/home/sources/diagnostics/grabas/fpga/prod_fofbnode/out/",
        "/home/sources/diagnostics/grabas/fpga/dev_fofbnode/out/",
        ]
##############################################

def get_path_prefix(t, tag, sort):

    files = []
    for d in OUT_DIRS:
        files += glob.glob(d+
                "FofbNode_{0}Node/FofbNode_{0}Node_{1}*.xsa".format(
                t.capitalize(), tag))

    if len(files) <1:
        raise ValueError("File pattern not found with this type and tag : {}, {} ".format(t.capitalize(), tag))

    if len(files) >1:
        if sort=='time':
            files= sorted(files, key=os.path.getctime)
        else:
            files= sorted(files)
        print("\nWarning: Several files with same type and tags:")
        for _f in files[:-1]:
            print("           "+os.path.split(_f)[-1])

        print(" select -> "+os.path.split(files[-1])[-1]+"\n")

    return files[-1][:-4]


def delete_old_link(t, name):
    links= os.listdir(VARIANTS_DIR)

    if name  != "":
        name=r"{}_".format(name)

    links = [l for l in links if re.search(r'FofbNode_{}Node_'.format(t.capitalize())+name+r"\d", l)]

    if len(links) >0:
        print("\nRemoving links:")
        for l in links:
            print(l)
            os.remove(VARIANTS_DIR+l)

def add_link(fpathprefix, name, ext):
    basename = os.path.basename(fpathprefix).split('_')
    if name != "":
        basename.insert(2, name)

    linkname = VARIANTS_DIR+"_".join(basename)+ext
    target = fpathprefix+ext

    print("\nInstall link:")
    print("     "+linkname)
    print("  -> "+target)

    os.symlink(target , linkname)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('cfg', choices=['central', 'cell'])
    parser.add_argument('tag', type=str)
    parser.add_argument('-n', type=str, default="", help="Version name. ie 'test'. This will be added after the configurationame. Default is no type added.")
    parser.add_argument('--sort', choices=['time', 'alphanum'], default='time', help="How to sort files, before selecting the first in the list. Default is 'time'")

    args = parser.parse_args()

    fpathprefix = get_path_prefix(args.cfg, args.tag, args.sort)

    delete_old_link(args.cfg, args.n)

    for ext in [".xsa", "_ch8.mapt"]:
        add_link(fpathprefix, args.n, ext)
